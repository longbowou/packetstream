# PacketStream

Residential Proxies Powered By Peer-To-Peer Bandwidth Sharing.

## How to run it?

- Clone the repository
- Run

```bash
docker compose up -d
```

- Logs

```bash
docker compose logs -f
```
